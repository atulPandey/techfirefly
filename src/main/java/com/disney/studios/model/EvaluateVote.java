package com.disney.studios.model;

import org.springframework.stereotype.Component;

@Component
public class EvaluateVote {
	public enum Vote {
		UP, DOWN;
	}

	public int getVote(Vote vote) {
		switch (vote) {
		case UP:
			return 1;
		case DOWN:
			return -1;
		}
		return 0;
	}

}
