package com.disney.studios.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "TBL_CLIENT")
@DynamicUpdate
public class ClientDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ClientId client;
	
	@ManyToOne
    @JoinColumns({
            @JoinColumn(name = "image_url", referencedColumnName = "image_url", insertable = false, updatable = false),
            @JoinColumn(name = "breeds", referencedColumnName = "breeds", insertable = false, updatable = false),
    })
    @JsonIgnore
    private PetDog petDog;
	
	@Column(name = "hasVoted",nullable = false, columnDefinition = "boolean default true")	
	private boolean hasVoted;

	public ClientId getClient() {
		return client;
	}

	public void setClient(ClientId client) {
		this.client = client;
	}

	public PetDog getPetDog() {
		return petDog;
	}

	public void setPetDog(PetDog petDog) {
		this.petDog = petDog;
	}

	public boolean hasVoted() {
		return hasVoted;
	}

	public void setHasVoted(boolean hasVoted) {
		this.hasVoted = hasVoted;
	}
	
	/*	@Column(name = "image_url")
	private String refImageUrl;
	
	@Column(name = "pet_id")
	private String refPetId;
	
	@Id
	@Column(name = "client_id")
	private Long clientId;
	
	@Id
	@ManyToOne(optional = false)
	@JoinColumns(value = { @JoinColumn(name = "refImageUrl", referencedColumnName = "image_url"),
			@JoinColumn(name = "refPetId", referencedColumnName = "petId") })
	private PetDog petDog;
	
	@Column(name = "hasVoted",nullable = false, columnDefinition = "boolean default false")	
	private boolean hasVoted;
	
*/}
