package com.disney.studios.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "TBL_DOGS")
@DynamicUpdate
public class PetDog implements Serializable {
	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long petId;
	 * 
	 * @Id
	 * 
	 * @Column(name = "image_url") String imageUrl;
	 */
	//@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private Long id;

	@EmbeddedId
	private PetId petId;


	@Column(name = "vote_count", nullable = false, columnDefinition = "int default 0")
	Integer voteCount;

	public PetDog() {

	}

	public PetId getPet() {
		return petId;
	}

	public void setPet(PetId pet) {
		this.petId = pet;
	}

	public Integer getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(Integer voteCount) {
		this.voteCount = voteCount;
	}

	/*public String getBreeds() {
		return breeds;
	}

	public void setBreeds(String breeds) {
		this.breeds = breeds;
	}*/
	
	@OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, mappedBy = "petDog")
    private Set<ClientDetails> clientDetails;

	@Override
	public String toString() {
		return "breed: " + petId.getBreeds() + " imageUrl: " + petId.getImageUrl() + " votes: " + voteCount;

	}
}
