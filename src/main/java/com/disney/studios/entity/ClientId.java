package com.disney.studios.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class ClientId implements Serializable {

	private static final long serialVersionUID = 1L;

	PetId petId;

	@Column(name = "client_id")
	private Long clientId;

	@Column(name = "client_name")
	private String clientName;

	public ClientId() { }

	public ClientId(PetId petId, Long clientId, String clientName) { 
		super(); 
		this.clientName = clientName;
		this.clientId = clientId; 
		this.petId = petId;
	}

	public PetId getPetId() {
		return petId;
	}

	public void setPetId(PetId petId) {
		this.petId = petId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	/*
	 * 
	 * private static final long serialVersionUID = 1L;
	 * 
	 * @Column(name = "client_id") private Long clientId;
	 * 
	 * @ManyToOne(optional = false)
	 * 
	 * @JoinColumns(value = { @JoinColumn(name = "refImageUrl",
	 * referencedColumnName = "image_url"),
	 * 
	 * @JoinColumn(name = "refPetId", referencedColumnName = "petId") }) private
	 * PetDog petDog;
	 * 
	 * public Client() { }
	 * 
	 * public Client(PetDog petDog, Long clientId) { super(); this.clientId =
	 * clientId; this.petDog = petDog; }
	 */}