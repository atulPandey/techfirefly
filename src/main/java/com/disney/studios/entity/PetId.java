package com.disney.studios.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Embeddable
public class PetId implements Serializable{
	
	private static final long serialVersionUID = 1L;

	
	@Column(name = "breeds")
	String breeds;
	
	
	@Column(name = "image_url")
	String imageUrl;

	public PetId(){
		
	}
	
	public PetId(String breeds, String imageUrl) {
		this.breeds = breeds;
		this.imageUrl = imageUrl;
	}


	public String getImageUrl() {
		return imageUrl;
	}


	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}


	public String getBreeds() {
		return breeds;
	}


	public void setBreeds(String breeds) {
		this.breeds = breeds;
	}


}
