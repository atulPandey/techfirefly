package com.disney.studios.exception;

public class VoteRegisteredException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public VoteRegisteredException() {
		super("Vote registered!!");
	}

	public VoteRegisteredException(String breedName,String imageUrl) {
		super("User has Already Voted for given breed:"+breedName+" image: "+imageUrl);
	}
}