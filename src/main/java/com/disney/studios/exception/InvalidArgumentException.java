package com.disney.studios.exception;

public class InvalidArgumentException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public InvalidArgumentException() {
		super("Argument seems to be invalid");
	}

	public InvalidArgumentException(String exceptionMessage) {
		super(exceptionMessage);
	}
}