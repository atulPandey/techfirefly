package com.disney.studios.exception;

public class NoDataFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public NoDataFoundException() {
		super("No data found for given input");
	}
	
	public NoDataFoundException(String message) {
		super(message);
	}
}
