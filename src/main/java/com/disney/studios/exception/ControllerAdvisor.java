package com.disney.studios.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor {

	@ExceptionHandler(InvalidArgumentException.class)
	public ResponseEntity<String> handleContentNotAllowedException(InvalidArgumentException exception) {
		return new ResponseEntity<>(new String(exception.getMessage()), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NoDataFoundException.class)
	public ResponseEntity<Object> handleNodataFoundException(NoDataFoundException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(VoteRegisteredException.class)
	public ResponseEntity<Object> handleNodataFoundException(VoteRegisteredException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.METHOD_NOT_ALLOWED);
	}

}
