package com.disney.studios.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.disney.studios.entity.ClientDetails;
import com.disney.studios.entity.ClientId;

public interface ClientRepository<P> extends JpaRepository<ClientDetails, ClientId> {
		Optional<ClientDetails> findByClientClientId(Long clientId);
		
		@Query(value = "SELECT * FROM TBL_CLIENT c where UPPER(breeds) = UPPER(:breedName)"
				+ "and UPPER(image_url) = UPPER(:imageUrls) and UPPER(client_id) = UPPER(:clientId)",nativeQuery = true)
		Optional<List<ClientDetails>> checkForExistingRecord(@Param("imageUrls")String imageUrl, @Param("breedName")String breedName,@Param("clientId")Long clientId);
		
		@Modifying
		@Query(value = "INSERT INTO TBL_CLIENT(breeds,image_url,client_id,client_name) VALUES(:breedName,:imageUrls,:clientId,:clientName)",nativeQuery = true)
		void insertData(@Param("imageUrls")String imageUrl, @Param("breedName")String breedName,@Param("clientId") Long clientId, @Param("clientName")String clientName);
	
}
