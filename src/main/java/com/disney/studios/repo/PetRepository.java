package com.disney.studios.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.disney.studios.entity.PetDog;
import com.disney.studios.entity.PetId;

public interface PetRepository<P> extends JpaRepository<PetDog, PetId> {
	@Query("SELECT p.petId.imageUrl, p.petId.breeds FROM PetDog AS p GROUP BY p.petId.breeds,p.petId.imageUrl")
	List<PetDog> groupByAndGetAllPets();
	
	@Query("SELECT p.petId.imageUrl FROM PetDog p where UPPER(p.petId.breeds) = UPPER(:breeds)")
	Optional<List<String>> findImageByBreeds(@Param("breeds") String breed);
	
	@Modifying
	@Query("UPDATE PetDog p set p.voteCount = p.voteCount+:voteVal where UPPER(p.petId.breeds) = UPPER(:breeds) and p.petId.imageUrl = :imageUrls")
	int updateVote(@Param("voteVal") int voteVal,@Param("breeds") String breed,@Param("imageUrls") String imageUrl);

	@Query("SELECT p FROM PetDog p where p.petId.imageUrl = :imageUrls")
	Optional<PetDog> findByImageUrl(@Param("imageUrls")String imageUrl);
}
