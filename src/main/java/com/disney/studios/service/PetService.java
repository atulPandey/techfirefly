package com.disney.studios.service;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import com.disney.studios.entity.ClientDetails;
import com.disney.studios.entity.ClientId;
import com.disney.studios.entity.PetDog;
import com.disney.studios.entity.PetId;
import com.disney.studios.exception.InvalidArgumentException;
import com.disney.studios.exception.NoDataFoundException;
import com.disney.studios.exception.VoteRegisteredException;
import com.disney.studios.model.Client;
import com.disney.studios.model.EvaluateVote;
import com.disney.studios.model.EvaluateVote.Vote;
import com.disney.studios.repo.ClientRepository;
import com.disney.studios.repo.PetRepository;

@Service
@Transactional
public class PetService {

	@Autowired
	private PetRepository<PetDog> petRepository;

	@Autowired
	private EvaluateVote evalVote;

	@Autowired
	private ClientRepository<ClientDetails> clientRepository;

	private static final Logger LOGGER = getLogger(PetService.class);

	public MultiValueMap<String, String> getAllPetImages() {
		LOGGER.info("getAllPets method called:");
		List<PetDog> lstofPetDogs = (List<PetDog>) petRepository.findAll();
		if (lstofPetDogs.isEmpty())
			throw new NoDataFoundException();
		MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
		lstofPetDogs.stream().forEach(pet -> {
			multiValueMap.add(pet.getPet().getBreeds(), pet.getPet().getImageUrl());
		});
		return multiValueMap;
	}

	public MultiValueMap<String, List<String>> getPets(String breedName) {
		LOGGER.info("getPets method called:");
		MultiValueMap<String, List<String>> multiValueMap = new LinkedMultiValueMap<>();
		List<String> retList = petRepository.findImageByBreeds(breedName).orElseThrow(NoDataFoundException::new);
		multiValueMap.add(breedName, retList);
		return multiValueMap;
	}

	public String recordVoteForPet(String vote, String breedName, String imageUrl, String clientName, Long clientId) {
		LOGGER.info("recordVoteForPet method called:");
		int voteVal = 0;
		int updateFlag = 0;
		try {
			if (checkIfUserHasVotedForImage(clientName, clientId, breedName, imageUrl))
				throw new VoteRegisteredException(breedName,imageUrl);
			Vote actualVote = Vote.valueOf(vote.toUpperCase());
			voteVal = evalVote.getVote(actualVote);
		} catch (IllegalArgumentException exception) {
			throw new InvalidArgumentException("please select valid option for Vote");
		}
		if (voteVal != 0)
			updateFlag = petRepository.updateVote(voteVal, breedName, imageUrl);
		return updateFlag == 1 ? new String("Vote recorded successfully ") : " The record couldnt be updated";
	}

	private boolean checkIfUserHasVotedForImage(String clientName, Long clientId, String breedName, String imageUrl) {
		LOGGER.info("checking if user has voted for the given image");
		// clientRepository.findBy(imageUrl,breedName,userDetails.getClientId());
		ClientDetails clientDetail = setClientData(clientName, clientId, breedName, imageUrl);
		List<ClientDetails> clientDetails = null;
		try {
			clientDetails = clientRepository.checkForExistingRecord(imageUrl, breedName, clientId).get();
		} catch (NoSuchElementException exception) {
			LOGGER.info("No mapping present for given data");
						
		}
		if (clientDetails == null){
			LOGGER.info("Adding new user details");
			try{
			clientRepository.insertData(imageUrl, breedName, clientId, clientName);
			}catch(DataIntegrityViolationException | ConstraintViolationException exception ){
				throw new InvalidArgumentException("please check the breedName or imageUrl");
			}
			return false;
		}
		return true;
	}

	private ClientDetails setClientData(String clientName, Long clientIds, String breedName, String imageUrl) {

		ClientDetails clientDetail = new ClientDetails();
		clientDetail.setHasVoted(false);
		PetId petId = new PetId(breedName, imageUrl);
		ClientId clientId = new ClientId(petId, clientIds, clientName);
		clientDetail.setClient(clientId);
		// lst.add(clientDetail);
		return clientDetail;
	}

	public PetDog getPetDetails(String imageUrl) {
		LOGGER.info("getPetDetails method called:");
		return petRepository.findByImageUrl(imageUrl).orElseThrow(NoDataFoundException::new);
	}
}
