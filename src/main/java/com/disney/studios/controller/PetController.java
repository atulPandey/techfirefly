package com.disney.studios.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.disney.studios.entity.PetDog;
import com.disney.studios.exception.InvalidArgumentException;
import com.disney.studios.model.Client;
import com.disney.studios.service.PetService;

@RestController
@RequestMapping(value = "/pet")
public class PetController {
	
	@Autowired
	PetService petService;		
	
	
	@GetMapping(value = "/all", produces = "application/json")
	public MultiValueMap<String, String> getAllPetImages() {
		return petService.getAllPetImages();
	}

	@GetMapping(value = "/get", produces = "application/json")
	public MultiValueMap<String, List<String>> getPets(@RequestParam(required = true) Optional<String> breedName) {
		return  petService.getPets(breedName.orElseThrow(InvalidArgumentException::new));
	}

	@GetMapping(value = "/getDetails", produces = "application/json")
	public PetDog getPetDetails(@RequestParam(required = true) Optional<String> imageUrl) {
		return petService.getPetDetails(imageUrl.orElseThrow(InvalidArgumentException::new));
	}
	
	@PostMapping(value = "/recordVote", produces = "application/json")
	public String voteForPet(@RequestParam(required = true) Optional<String> recordedVote,
			@RequestParam(required = true) Optional<String> breedName,
			@RequestParam(required = true) Optional<String> imageUrl,
			@RequestParam(required = true) Optional<String> clientName,
			@RequestParam(required = true) Optional<Long> clientId) {
		String retString =petService.recordVoteForPet(recordedVote.orElseThrow(InvalidArgumentException::new), 
				breedName.orElseThrow(InvalidArgumentException::new), 
				imageUrl.orElseThrow(InvalidArgumentException::new),
				clientName.orElseThrow(InvalidArgumentException::new),clientId.orElseThrow(InvalidArgumentException::new));
		return retString;
	}
	
	 

}
