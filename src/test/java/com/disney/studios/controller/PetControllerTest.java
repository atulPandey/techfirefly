package com.disney.studios.controller;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.disney.studios.entity.PetDog;
import com.disney.studios.exception.InvalidArgumentException;
import com.disney.studios.service.PetService;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class PetControllerTest {

	@InjectMocks
	PetController petController;
	@Mock
	PetService petService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void test_getAllPetImages_success() {
		MultiValueMap<String, String> expectedMap = new LinkedMultiValueMap<>();
		Mockito.when(petController.getAllPetImages()).thenReturn((expectedMap));
		MultiValueMap<String, String> actualMap = petController.getAllPetImages();
		Assert.assertEquals(actualMap, expectedMap);
	}

	@Test
	public void test_getPets_throwException() {
		Optional<String> breedName = Optional.ofNullable("");
		Mockito.when(petController.getPets(breedName)).thenThrow(InvalidArgumentException.class);
		Assertions.assertThrows(InvalidArgumentException.class, () -> petController.getPets(breedName));
	}

	@Test
	public void test_getPets_success() {
		MultiValueMap<String, List<String>> expectedMap = new LinkedMultiValueMap<>();
		Optional<String> breedName = Optional.ofNullable(Mockito.anyString());
		Mockito.when(petController.getPets(breedName)).thenReturn(expectedMap);
		MultiValueMap<String, List<String>> actualMap = petController.getPets(breedName);
		Assertions.assertEquals(actualMap, expectedMap);
	}

	@Test
	public void test_getPetDetails_success() {
		PetDog expectedPet = new PetDog();
		Optional<String> imageUrl = Optional.ofNullable(Mockito.anyString());
		Mockito.when(petController.getPetDetails(imageUrl)).thenReturn(expectedPet);
		PetDog actualPet = petController.getPetDetails(imageUrl);
		Assertions.assertEquals(actualPet, expectedPet);
	}

	@Test
	public void test_getPetDetails_throwException() {
		Optional<String> imageUrl = Optional.ofNullable("");
		Mockito.when(petController.getPetDetails(imageUrl)).thenThrow(InvalidArgumentException.class);
		Assertions.assertThrows(InvalidArgumentException.class, () -> petController.getPetDetails(imageUrl));
	}

	@Test
	public void test_voteForPet_throwException() {
		Mockito.when(petController.voteForPet(Optional.ofNullable(""), Optional.ofNullable(""),
				 Optional.ofNullable(""), Optional.ofNullable(""), Optional.ofNullable(new Long(10)))).thenThrow(InvalidArgumentException.class);
		Assertions.assertThrows(InvalidArgumentException.class,()-> petController.voteForPet(Optional.ofNullable(""), Optional.ofNullable(""),
				 Optional.ofNullable(""), Optional.ofNullable(""), Optional.ofNullable(new Long(10))));
	}

}
