package com.disney.studios.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.disney.studios.entity.PetDog;
import com.disney.studios.entity.PetId;
import com.disney.studios.exception.NoDataFoundException;
import com.disney.studios.repo.PetRepository;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class PetServiceTest {

	@InjectMocks
	PetService petService;

	@Mock
	PetRepository petRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	private List<PetDog> setPetDogData() {
		List<PetDog> lst = new ArrayList<>();
		PetDog petDog = new PetDog();
		PetId petId = new PetId("Pug", "https://www.google.com/idx");
		petDog.setPet(petId);
		lst.add(petDog);
		return lst;
	}

	@Test
	public void test_getAllPetImages_success() {
		List<PetDog> lst = setPetDogData();
		MultiValueMap<String, String> expectedMap = new LinkedMultiValueMap<>();
		expectedMap.add("Pug", "https://www.google.com/idx");
		Mockito.when(petRepository.findAll()).thenReturn(lst);
		MultiValueMap<String, String> actualMap = petService.getAllPetImages();
		Assertions.assertEquals(expectedMap, actualMap);
	}

	@Test
	public void test_getAllPetImages_throwException() {
		Mockito.when(petRepository.findAll()).thenReturn(Collections.emptyList());
		Assertions.assertThrows(NoDataFoundException.class, () -> petService.getAllPetImages());
	}

	@Test
	public void test_getPets_throwException() {
		Mockito.when(petRepository.findImageByBreeds(Mockito.anyString())).thenThrow(NoDataFoundException.class);
		Assertions.assertThrows(NoDataFoundException.class, () -> petService.getPets(Mockito.anyString()));
	}
	
	@Test
	public void test_getPets_success() {
		Mockito.when(petRepository.findImageByBreeds(Mockito.anyString())).thenThrow(NoDataFoundException.class);
		Assertions.assertThrows(NoDataFoundException.class, () -> petService.getPets(Mockito.anyString()));
	}

}
